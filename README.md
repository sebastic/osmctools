A few really fast tools to convert, filter and update OpenStreetMap data files<br/>
<br/>
Current Version: 0.8<br/>
<br/>
Programm desrcriptions:<br/>
https://wiki.openstreetmap.org/wiki/osmconvert <br/>
https://wiki.openstreetmap.org/wiki/osmfilter <br/>
https://wiki.openstreetmap.org/wiki/osmupdate <br/>
